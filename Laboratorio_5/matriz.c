#include<stdio.h>
#include<stdlib.h>
#include <matriz.h>

int main() {
	int m,n,i,j;
	printf("Vamos a crear una matriz de dimensiones mxn: \n");
	printf("Cantidad de filas: \n");
	scanf("%d", &m);
	printf("Cantidad de columnas: \n");
	scanf("%d", &n);
	int **pointer= crear(m, n);
	int **sumada=suma(m,n,pointer);
	int **multip=multi(m,n,pointer);
	int **sumesc=sumaesc(m,n,pointer);
	int **multesc=multiesc(m,n,pointer);
	int **transpuesta=transponer(m,n,pointer);
        int **w= limpiar (m, n, pointer);

	return(0);
}
