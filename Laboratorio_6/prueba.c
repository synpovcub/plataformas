
#include <stdio.h>

#include <stdlib.h>

#include <ctype.h>
//#include <maze.h>
#include <stdbool.h>



int m[14][40];


int f,c; // filas y columnas del laberinto

int fp,cp; // coordenadas del jugador

int fs,cs; // coordenadas de la salida o meta

int  const ARRIBA=0, DERECHA=1, IZQUIERDA=2, ABAJO=3;

int direccion=0;

int arriba, abajo, izquierda, derecha;



void leerArchivo() {

     int maxCol;

    FILE *archivo;

    char caracter;

    int codigo;

    bool flag=true;



        archivo=fopen("maze.txt","rt");


        while(!feof(archivo)) { // Este ciclo sirve para dimensionar nuestra matriz

                caracter=fgetc(archivo);

                codigo = caracter; // obtenemos el codigo Ascii de el caracter



                if (codigo==10) { // codigo del salto de linea

                   f++; // Si encontramos un enter añadimos una fila mas a nuestra matriz

                   if (flag) {maxCol=c; flag=false;} // solo lo hacemos una vez

                   c=0;

                } else {

                       m[f][c]=codigo-48;

                       if (m[f][c]==2){fp=f;cp=c;}

                       if (m[f][c]==3){fs=f;cs=c;}

                       c++ ;// añadimos una columna mas a nuestra matriz

                }

        }

        c=40; // si no ponemos esto la matriz queda de una columna

        fclose(archivo);


}



void MuestraLaberinto() {



    for (int i=0;i<f;i++) {

        for (int ii=0;ii<c;ii++) {

           if (m[i][ii]==1) {printf ("#") ;}

           else if (i==fs && ii==cs) printf ("Q");

           else if (i==fp && ii==cp) printf ("R");

           else printf (" ");

        }

    printf ("\n");

    }
}


void BuscarQueso(){

    int old_fp,old_cp; 

      old_fp=fp; old_cp=cp;



      arriba=m[fp-1][cp];

      abajo=m[fp+1][cp];

      derecha=m[fp][cp+1];

      izquierda=m[fp][cp-1];



      // verificar si la meta esta junto al raton

      if (derecha==3) direccion=DERECHA;

      else if (izquierda==3) direccion=IZQUIERDA;

      else if (arriba==3) direccion=ARRIBA;

      else if (abajo==3) direccion=ABAJO;



      // Si no estamos junto a la meta, avanzamos

      else if (direccion==ARRIBA){ // volteando al norte

        if (derecha==1 && arriba==0) {}// Antes de avanzar checamos si hay pared a la derecha

         else if (derecha==0) direccion=DERECHA;// si no hay pared a la derecha giramos viendo a la derecha

         else if (izquierda==0) direccion=IZQUIERDA;// si no, si no hay pared en la izq. giramos viendo hacia alla

         else direccion=ABAJO;// ultima opcion, abajo giramos viendo hacia abajo

      }

      else if (direccion==1){ // volteando al este/derecha

         if (abajo==1 && derecha==0) {}// Antes de avanzar hacia la derecha checamos si hay pared abajo

         else if (abajo==0) direccion=ABAJO;// si no hay pared abajo giramos a la derecha, mirando abajo

         else if (arriba==0) direccion=ARRIBA;// si no hay pared arriba giramos

         else direccion=IZQUIERDA;// ultima opcion la izquierda

      }

      else if (direccion==3){ // volteando al sur

         if (abajo==0 && izquierda==1) {}// si no hay pared abajo y a la izq. si, avanzamos

         else if (izquierda==0) direccion=IZQUIERDA;// si no hay pared a la izquierda giramos

         else if (derecha==0) direccion=DERECHA;// si no hay pared a la derecha giramos

         else direccion=ARRIBA; // ultima opcion arriba

      }

      else if (direccion==2){ // volteando a la izquierda

         if (izquierda==0 && arriba==1) {}// si hay pared arriba y a la izq. no, avanzamos             

         else if (arriba==0) direccion=ARRIBA;// si no hay pared arriba giramos a la derecha,

         else if (abajo==0) direccion=ABAJO;// si no hay pared abajo giramos 

         else direccion=DERECHA;// ultima opcion volver atras, giramos 

      }

 

      m[old_fp][old_cp]=0; // ponemos un espacio donde estaba el jugador

 

      switch(direccion){ // Movemos al jugador

        case 0:

             m[fp-1][cp]=2; // actualizamos matriz

            fp--; // Actualizamos coordenadas del jugador

        break;

        case 1:

             m[fp][cp+1]=2; // actualizamos matriz

             cp++; // Actualizamos coordenadas del jugador

        break;

        case 2:

             m[fp][cp-1]=2; // actualizamos matriz

            cp--; // Actualizamos coordenadas del jugador

        break;

        case 3:

             m[fp+1][cp]=2; // actualizamos matriz

            fp++; // Actualizamos coordenadas del jugador

        break;

      }

}



int main() {

    leerArchivo();
	//readMaze();

    MuestraLaberinto();

   while (fp!=fs || cp!=cs) {// Cuando encontramos la meta, salimos del ciclo

    BuscarQueso();

     MuestraLaberinto();

   }

   // system("clear");

    printf ("El raton encontro el queso\n");

    system("pause");

    return 0;

}
